﻿(function () {
    'use strict';
    angular
        .module('ecommerceApp')
        .controller('productsController', function ($scope, $http, $log) {

            $http.get('http://localhost:2022/api/products')
        .success(function (result) {
            $scope.products = result;
        })
            .error(function (data, status) {
                $log.error(data);
            });

        });

})();