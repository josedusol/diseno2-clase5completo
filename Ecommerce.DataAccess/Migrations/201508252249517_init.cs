namespace Ecommerce.DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class init : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Orders",
                c => new
                    {
                        OrderID = c.Int(nullable: false, identity: true),
                        DateCreated = c.DateTime(nullable: false),
                        DateConfirmed = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.OrderID);
            
            CreateTable(
                "dbo.OrderLines",
                c => new
                    {
                        OrderLineId = c.Int(nullable: false, identity: true),
                        Amount = c.Double(nullable: false),
                        Product_ProductID = c.Int(),
                        Order_OrderID = c.Int(),
                    })
                .PrimaryKey(t => t.OrderLineId)
                .ForeignKey("dbo.Products", t => t.Product_ProductID)
                .ForeignKey("dbo.Orders", t => t.Order_OrderID)
                .Index(t => t.Product_ProductID)
                .Index(t => t.Order_OrderID);
            
            CreateTable(
                "dbo.Products",
                c => new
                    {
                        ProductID = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        Descrpition = c.String(),
                        Price = c.Double(nullable: false),
                    })
                .PrimaryKey(t => t.ProductID);
            
            CreateTable(
                "dbo.Features",
                c => new
                    {
                        FeatureID = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        Descrpition = c.String(),
                        Product_ProductID = c.Int(),
                    })
                .PrimaryKey(t => t.FeatureID)
                .ForeignKey("dbo.Products", t => t.Product_ProductID)
                .Index(t => t.Product_ProductID);
            
            CreateTable(
                "dbo.Users",
                c => new
                    {
                        UserId = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        LastName = c.String(),
                        Email = c.String(),
                        Password = c.String(),
                        State = c.Int(nullable: false),
                        Address_Street = c.String(),
                        Address_State = c.String(),
                        Address_Country = c.String(),
                        Address_DoorNumber = c.Int(nullable: false),
                        Address_ZipCode = c.Int(nullable: false),
                        Address_Phone = c.String(),
                    })
                .PrimaryKey(t => t.UserId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.OrderLines", "Order_OrderID", "dbo.Orders");
            DropForeignKey("dbo.OrderLines", "Product_ProductID", "dbo.Products");
            DropForeignKey("dbo.Features", "Product_ProductID", "dbo.Products");
            DropIndex("dbo.Features", new[] { "Product_ProductID" });
            DropIndex("dbo.OrderLines", new[] { "Order_OrderID" });
            DropIndex("dbo.OrderLines", new[] { "Product_ProductID" });
            DropTable("dbo.Users");
            DropTable("dbo.Features");
            DropTable("dbo.Products");
            DropTable("dbo.OrderLines");
            DropTable("dbo.Orders");
        }
    }
}
